'use strict';
function myscroll(element) {
  $('html, body').animate({scrollTop: $(element).offset().top}, 1000);
}
$('.cancel-btn').click(function() {
  $('.lightbox, .polythene').css({display: 'none'});
  $('html,body').css({'overflow-y': 'scroll'});
});
$('.open-menu').click(function() {
  $('.small-menu').addClass('sm-opn');
  $('.cancel-menu').show();
  $('.open-menu').hide();
});
$('.cancel-menu').click(function() {
  $('.small-menu').removeClass('sm-opn');
  $('.cancel-menu').hide();
  $('.open-menu').show();
});
